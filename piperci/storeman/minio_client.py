from urllib.parse import urlparse

from minio import Minio
from minio.error import (BucketAlreadyExists, BucketAlreadyOwnedByYou,
                         MinioError)

from piperci.storeman.client import BaseStorageClient
from piperci.storeman.exceptions import StoremanError


class MinioClient(BaseStorageClient):
    def __init__(self, *args, **kwargs):
        self.hostname = kwargs.get("hostname")
        access_key = kwargs.get("access_key")
        secret_key = kwargs.get("secret_key")
        self.storage_client = Minio(
            self.hostname, access_key=access_key, secret_key=secret_key, secure=False
        )

    def stat_file(self, bucket_name, prefix=None, recursive=False):
        try:
            return self.storage_client.list_objects(
                bucket_name, prefix=prefix, recursive=recursive
            )
        except MinioError as e:
            raise StoremanError(e)

    def download_file(self, uri, file_path):
        scheme = urlparse(uri).scheme
        if scheme != "minio":
            raise ValueError(f"Unknown URI scheme: {scheme}")
        try:
            bucket_name = urlparse(uri).path.split("/")[1]
            object_name = "/".join(urlparse(uri).path.split("/")[2:])
        except IndexError as e:
            raise StoremanError(f"Invalid URI detected. {e}")

        try:
            return self.storage_client.fget_object(bucket_name, object_name, file_path)
        except MinioError as e:
            raise StoremanError(e)

    def upload_file(self, bucket_name, object_name, file_path):
        # Check type of file_path and either fput or put_object based on file or stream
        try:
            self.storage_client.make_bucket(bucket_name)
        except (BucketAlreadyExists, BucketAlreadyOwnedByYou):
            pass
        except MinioError as e:
            raise StoremanError(e)
        self.storage_client.fput_object(bucket_name, object_name, file_path)
        return self.storage_client.stat_object(bucket_name, object_name)

    def gen_file_uri(self, bucket_name, object_name):
        return f"minio://{self.hostname}/{bucket_name}/{object_name}"
