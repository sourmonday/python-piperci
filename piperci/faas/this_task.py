import json
import logging
import os

import requests

from pythonjsonlogger import jsonlogger

from piperci.artman import artman_client
from piperci.faas.exceptions import (
    PiperActiveError,
    PiperDelegateError,
    PiperLoggingError,
)
from piperci.gman import client as gman_client
from piperci.sri import generate_sri
from piperci.storeman.client import storage_client
from piperci.storeman.exceptions import StoremanError


class ThisTask:
    def __init__(
        self,
        gman_url,
        *,
        storage,
        project,
        thread_id=None,
        parent_id=None,
        run_id,
        status,
        stage,
        caller,
        log_file="faas.log",
    ):
        """
        Representation of a task object from GMan.
        :param gman_url: URL for GMan
        :param storage: Dict containing storage parameters
        :param project: Project name
        :param thread_id: Thread to associate this task with
        :param parent_id: Parent task ID to associate this task with
        :param run_id: Run to associate this task with
        :param status: Status of the task. Must be "started" or "received"
        :param stage: The name of the stage that this task is associated with.
        :param caller: The name of the program that created this task.
        """
        self.gman_url = gman_url
        self.stage = stage
        self.project = project
        self.thread_id = thread_id
        self.parent_id = parent_id
        if run_id:
            self.run_id = str(run_id)
        else:
            raise PiperActiveError("run_id must not be None")
        self.status = status
        self.caller = caller
        self.log_file = log_file
        self.logger = self._init_logger(log_file=self.log_file)
        self.storage_client = self._init_storage_client(**storage)
        self.task = self._start(
            project=project,
            thread_id=thread_id,
            parent_id=parent_id,
            run_id=run_id,
            status=status,
            caller=caller,
        )

    def _init_logger(self, log_level="info", log_file="faas.log"):
        """
        Initializes the Python logger
        :param log_level: Sets the log level for all handlers
        :return: Reference to the logger
        """
        logger = logging.getLogger(__name__)
        logger.setLevel(log_level.upper())

        stream = logging.StreamHandler()
        stream.setLevel(log_level.upper())
        formatter = jsonlogger.JsonFormatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s", timestamp=True
        )
        stream.setFormatter(formatter)

        file = logging.FileHandler(log_file)
        file.setLevel(log_level.upper())
        formatter = jsonlogger.JsonFormatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s", timestamp=True
        )
        file.setFormatter(formatter)

        logger.addHandler(stream)
        logger.addHandler(file)

        return logger

    def _init_storage_client(self, storage_type, **kwargs):
        """
        Initializes the storage client.
        :param storage_type: The type of storage that will be used
        :param kwargs: Required KWargs depend on the storage type.
          For Minio:
          hostname: The hostname of the storage service.
          access_key: The access key for the storage service.
          secret_key: The secret key for the storage service
        :return: piperci.storeman.client.BaseStorageClient
        """

        minio_client = storage_client(
            storage_type=storage_type,
            hostname=kwargs.get("hostname"),
            access_key=kwargs.get("access_key"),
            secret_key=kwargs.get("secret_key"),
            secure=False,
        )
        return minio_client

    def _start(self, *, project, thread_id, parent_id, run_id, status, caller):
        """
        Requests a task from GMan
        :param project: The project name that the task is associated with.
        :param thread_id: The thread that the task is associated with. Can be none.
        :param parent_id: The parent task ID. Can be none.
        :param status: The status to initialize the task with.
        Must be "received" or "started"
        :param caller: The caller of the task.
        :return: Task object from GMan
        """
        self.logger.info(f"Requesting task from GMan")

        data = {
            "gman_url": self.gman_url,
            "run_id": run_id,
            "project": project,
            "thread_id": thread_id,
            "status": status,
            "caller": caller,
        }
        if parent_id:
            data.update({"parent_id": parent_id})

        self.logger.debug(f"Params {data}")
        try:
            task = gman_client.request_new_task_id(**data)
            self.logger.info(f"Received new task {task}")
            return task
        except requests.RequestException as e:
            self.logger.error(
                f"Encountered unknown requests exception while trying to "
                f"request new task. {e}"
            )
            raise PiperActiveError(e)
        except ValueError as e:
            self.logger.error(
                f"ValueError received while trying to request a new task. {e}"
            )
            raise PiperActiveError(e)

    def _upload_file(self, file, *, filetype):
        """
        Uploads a file to the storage and tells ArtMan that a file
        has been uploaded with a given URI
        :param file: The path to the file that is being uploaded
        :param filetype: The type of the file. "log" or "artifact"
        :return: None
        """
        try:
            self.logger.info(
                f"Uploading file {file} to {self.get_artifact_uri(file, filetype)}"
            )
            bucket, object_name = self.get_artifact_uri(file, filetype, False)
            self.storage_client.upload_file(bucket, object_name, file)
        except StoremanError as e:
            self.logger.error(
                f"Encountered unknown exception while trying to upload files to storage."
                f"{e}"
            )
            raise PiperLoggingError(e)
        self.info(f"Uploaded file {file}")
        project_artifact_hash = generate_sri(file)
        artifact_uri = self.get_artifact_uri(file, filetype)
        self.logger.info(f"Artifact uploaded at {artifact_uri}")
        try:
            artman_client.post_artifact(
                task_id=self.task["task"]["task_id"],
                artman_url=self.gman_url,
                uri=artifact_uri,
                caller=self.caller,
                type=filetype,
                sri=str(project_artifact_hash),
            )
        except requests.RequestException as e:
            message = (
                f"Encountered unknown requests exception while trying to"
                f"post artifact to ArtMan. {e}"
            )
            self.logger.error(message)
            raise PiperLoggingError(e)

    def artifact(self, path):
        """
        Uploads artifacts at a given path
        If the path is a directory then we will walk the directory
        and upload all files found.
        :param path: File or directory to upload
        :return: None
        """
        self.info(f"Uploading artifact(s) located at {path}")
        if os.path.isdir(path):
            for root, dir, files in os.walk(path):
                for file in files:
                    self._upload_file(
                        file, filetype="artifact"
                    )

        elif os.path.isfile(path):
            self._upload_file(
                path, filetype="artifact"
            )
        else:
            self.info(f"Could not upload file(s) at {path}. Not a directory or file")

    def complete(self, message):
        """
        Completes the task with a given message
        :param message: Message to send to GMan on task completion.
        :return: A reference to the task and 200
        """
        self.logger.info(message)
        try:
            task_id = self.task["task"]["task_id"]
            self._upload_file(
                self.log_file, filetype="log"
            )
            gman_client.update_task_id(
                task_id=task_id,
                gman_url=self.gman_url,
                status="completed",
                message=message,
            )
            return json.dumps(self.task), 200
        except requests.exceptions.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception when trying to complete task. {e}"
            )
            raise PiperLoggingError(e)

    def delegate(self, url, data):
        """
        Attempts to delegate a task to another FaaS, usually an executor
        Raises PiperDelegateError if this fails
        Marks a task as delegated
        :return: None
        """
        self.info(f"Attempting to delegate execution to {url}")
        headers = {"Content-Type": "application/json"}
        data["stage"] = self.stage
        data.update(self.task["task"])
        data["parent_id"] = self.task["task"]["task_id"]
        data["thread_id"] = self.task["task"]["thread_id"]
        try:
            r = requests.post(url, data=json.dumps(data), headers=headers)
            r.raise_for_status()
        except requests.exceptions.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception when trying to delegate task. {e}"
            )
            raise PiperDelegateError(e)

        try:
            gman_client.update_task_id(
                task_id=self.task["task"]["task_id"],
                gman_url=self.gman_url,
                status="delegated",
                message=f"delegated execution to {url}",
            )
        except requests.exceptions.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception when updating task"
            )
            raise PiperDelegateError(e)

        self.info(f"Successfully delegated task {self.task} to {url}.")

    def fail(self, message):
        """
        Marks a task as failed in GMan and sends 422 to client
        :param message: Message to pass to the logger and GMan
        :return:
        """
        self.logger.error(message)
        try:
            self._upload_file(
                self.log_file, filetype="log"
            )
            gman_client.update_task_id(
                gman_url=self.gman_url,
                status="failed",
                task_id=self.task["task"]["task_id"],
                message=message,
            )
        except requests.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception when trying to mark task failed."
                f"{e}"
            )
            raise PiperLoggingError(e)
        return message, 422

    def info(self, message):
        """
        Logs an info message to GMan
        :param message:
        :return:
        """
        self.logger.info(message)
        try:
            gman_client.update_task_id(
                gman_url=self.gman_url,
                status="info",
                task_id=self.task["task"]["task_id"],
                message=message,
            )
        except requests.RequestException as e:
            self.logger.error(
                f"Encountered unknown request exception while trying to log to GMan. {e}"
            )
            raise PiperLoggingError(e)

    def logfile(self, path):
        """
        Uploads logs to minio/artman using a given path
        :return:
        """
        self.info(f"Uploading logs located at {path}")
        if os.path.isdir(path):
            for root, dir, files in os.walk(path):
                for file in files:
                    self._upload_file(
                        file, filetype="log"
                    )

        elif os.path.isfile(path):
            self._upload_file(path, filetype="log")
        else:
            self.info(f"Could not upload file(s) at {path}. Not a directory or file")

    def get_artifact_uri(self, file, filetype, fqdn=True):
        hyphenate_bad_chars = str.maketrans('_ ', '--')
        container = f"run-{self.run_id.translate(hyphenate_bad_chars)}"
        object_name = f"{filetype}/{self.stage}/{file}"
        if fqdn:
            return self.storage_client.gen_file_uri(container, object_name)
        else:
            return (container, object_name)
