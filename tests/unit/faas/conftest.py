import pytest


@pytest.fixture(scope="session")
def log_directory_fixture(tmpdir_factory):
    logdir = tmpdir_factory.mktemp("logs")
    for i in range(0, 5):
        log_file = logdir.join(f"log_file_{i}.txt")
        log_file.write("logs")
    return str(logdir)


@pytest.fixture(scope="session")
def log_file_fixture(tmpdir_factory):
    log_file = tmpdir_factory.mktemp("logs").join("log_file.txt")
    log_file.write("logs")
    return str(log_file)


@pytest.fixture
def request_new_task_patch(mocker):
    mocker.patch("piperci.gman.client.request_new_task_id")


@pytest.fixture
def requests_post_patch(mocker):
    mocker.patch("piperci.faas.this_task.requests.post")


@pytest.fixture
def upload_file_patch_to_noop(mocker):
    mocker.patch("piperci.storeman.minio_client.MinioClient.upload_file")


@pytest.fixture
def artman_patch(mocker):
    mocker.patch("piperci.artman.artman_client.post_artifact")


@pytest.fixture
def update_task_id_patch(mocker):
    mocker.patch("piperci.gman.client.update_task_id")


@pytest.fixture
def storage_fixture():
    storage = {
        "storage_type": "minio",
        "hostname": "localhost",
        "access_key": "1234",
        "secret_key": "1234",
    }
    return storage
